package net.camtech.fopmremastered;

import org.bukkit.plugin.java.JavaPlugin;

public class FreedomOpModRemasteredConfigs extends JavaPlugin
{
    public static FreedomOpModRemasteredConfigs plugin;
    public static FOPMR_Configs configs;
    
    @Override
    public void onEnable()
    {
        plugin = this;
        configs = new FOPMR_Configs();
    }
    
    @Override
    public void onDisable()
    {
        
    }
}
